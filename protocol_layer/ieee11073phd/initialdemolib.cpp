#include "log.h"
#include "PAL/Interface/personalhealthdevice.h"
#include "../Interface/simpledemowrapper.h"
#include <csignal>

#include <QtCore/QCoreApplication>
#include <QObject>
#include <QString>
#include <QVector>
#include <QDateTime>

#include <iostream>

DECLARE_LOG_MODULE("Main")

#include "log.h"
#include "../PAL/Interface/personalhealthdevice.h"
#include "../Interface/simpledemowrapper.h"
#include <csignal>

#include <QtCore/QCoreApplication>
#include <QObject>
#include <QString>
#include <QVector>
#include <QDateTime>

#include <iostream>

//DECLARE_LOG_MODULE("Main")

using std::cout;
using std::endl;
using std::string;

using PAL::PersonalHealthDeviceListener;
using PAL::PersonalHealthDevice;





char hexNibble (unsigned char c) {
    if (c > 9) {
        return 'A' + c - 10;
    } else {
        return '0' + c;
    }
}

// Convert a byte to hex-string
string hexByte(unsigned char c) {
    char o[2];
    o[0] = hexNibble((c >> 4) & 0xf);
    o[1] = hexNibble(c & 0xf);
    return string(o,2);
}

// Convert an AbsoluteTime message section to string
string timestamp(std::vector<unsigned char> *message, int start) {
    string retval;
    retval += hexByte((*message)[start]);
    retval += hexByte((*message)[start+1]);
    retval += "-";
    retval += hexByte((*message)[start+2]);
    retval += "-";
    retval += hexByte((*message)[start+3]);
    retval += " ";

    retval += hexByte((*message)[start+4]);
    retval += ":";
    retval += hexByte((*message)[start+5]);
    retval += ":";
    retval += hexByte((*message)[start+6]);
    retval += ".";
    retval += hexByte((*message)[start+7]);

    return retval;
}

// Convert a SFLOAT message type to string
string sfloat(u_int16_t val) {
    if (val >= 0x07FE && val <= 0x0802) {
        return "null"; // +/-Inf, NaN etc.
    }
    int16_t exponent = val & 0xF000;
    exponent = exponent / 4096;
    int16_t mantissa = val << 4;
    mantissa = mantissa / 16;
    string rval = QString::number(mantissa).toStdString();
    if (exponent) {
        rval += "e";
        rval += QString::number(exponent).toStdString();
    }
    return rval;
}

// Hex-print message on stdout
void printMsg(std::vector<unsigned char> *message) {
    cout << message->size() <<" bytes:"<<endl;
    for (std::vector<unsigned char>::iterator it = message->begin(); it != message->end(); ) {
        for (int i = 0; i<16 && it != message->end(); i++,it++) {
            if (i != 0)  cout << ", ";
            cout << hexByte(*it);
        }
        cout << endl;
    }
}

void sendEvent(string output);



struct ConnectedDevice {
    char state;
    string guid, manufacturer, model, serialno, oldMeasurementB, oldMeasurementP;
    u_int64_t guidval;
    std::shared_ptr<PersonalHealthDevice> phdevice; // FIXME: Should be initialized in constructor
    std::vector<string> measurementQueue;
    qint64 timeDiff;

    ConnectedDevice() : state(0), manufacturer("null"), model("null"), serialno("null"), oldMeasurementB("null"), oldMeasurementP("null") {}

    void wrapMDS(string);
    void sendNoMeasurement();
    void weAreConnected();
    void disassociate();
};

bool validTimestamp(std::vector<unsigned char> *message, int start) {
    QDateTime deviceTime = QDateTime::fromString((timestamp(message,start)+"0").c_str(),"yyyy-MM-dd hh:mm:ss.zzz");
    qint64 timeDiff = deviceTime.secsTo(QDateTime::currentDateTime());
    if (timeDiff < -60 || timeDiff > 60) {
        // More than 60 seconds off
        return false;
    }
    return true;
}

void ConnectedDevice::wrapMDS(string measurement) {
    if (state == 2 && measurement != "null") {
        if (measurement.find("18948") != string::npos) {
            oldMeasurementB = measurement;
        } else {
            oldMeasurementP = measurement;
        }
        return;
    }
    if (state == 3 && oldMeasurementB != "null") {
        string dimstring =
                "{ \"type\" : 4103, \"id\" : " + guid +
                ", \"manufacturer\" : "        + manufacturer +
                ", \"model\" : "               + model +
                ", \"serialNumber\" : "        + serialno +
                ", \"state\" : "               + "2" +
                ", \"measurement\" : "         + oldMeasurementB + " }";
        sendEvent(dimstring);
        oldMeasurementB = "null";
    }
    if (state == 3 && oldMeasurementP != "null") {
        string dimstring =
                "{ \"type\" : 4103, \"id\" : " + guid +
                ", \"manufacturer\" : "        + manufacturer +
                ", \"model\" : "               + model +
                ", \"serialNumber\" : "        + serialno +
                ", \"state\" : "               + "2" +
                ", \"measurement\" : "         + oldMeasurementP + " }";
        sendEvent(dimstring);
        oldMeasurementP = "null";
    }
    string dimstring =
            "{ \"type\" : 4103, \"id\" : " + guid +
            ", \"manufacturer\" : "        + manufacturer +
            ", \"model\" : "               + model +
            ", \"serialNumber\" : "        + serialno +
            ", \"state\" : "               + QString::number(state).toStdString() +
            ", \"measurement\" : "         + measurement + " }";
    sendEvent(dimstring);
}

void ConnectedDevice::sendNoMeasurement() {
    wrapMDS("null");
}

void ConnectedDevice::weAreConnected() {
    state = 2;
    sendNoMeasurement();
    if (!measurementQueue.empty()) {
        for (unsigned int i = 0; i < measurementQueue.size(); i++) {
            wrapMDS(measurementQueue[i]);
        }
        measurementQueue.clear();
    }
}

void ConnectedDevice::disassociate() {
    if (state == 2) { // Connected
        state = 3;
        sendNoMeasurement();
    }
    unsigned char x[] = {

        0xE4, 0x00,
        0x00, 0x02,
        0x00, 0x00

    };
    std::vector<unsigned char> m(x,x+sizeof(x));
    phdevice->sendMessagePrimary(&m);
}



ConnectedDevice defaultDevice; // FIXME: Delete and substitute by Map

class PHDListener : public PersonalHealthDeviceListener {
public:
    void connectIndication(std::shared_ptr<PersonalHealthDevice> device) {
        DEBUG("Connect Indication");
        ConnectedDevice &connDev = defaultDevice; // Lookup and create new if no existing
        (void) device; // Suppress compiler warning
        (void) connDev;
    }

    void disconnectIndication(std::shared_ptr<PersonalHealthDevice> device) {
        DEBUG("Disconnect indication");
        (void) device; // Suppress compiler warning
        ConnectedDevice &connDev = defaultDevice; // Lookup and return if not found

        if (connDev.state == 2) { // Connected
            INFO("DIM SIGNAL: BPM disconnecting");
            connDev.state = 3;
            connDev.sendNoMeasurement();
        }
        if (connDev.state == 3 || connDev.state == 1) {
            INFO("DIM SIGNAL: BPM disconnected");
            connDev.state = 0;
            connDev.sendNoMeasurement();
        }

    }

    void messageReceived(std::shared_ptr<PersonalHealthDevice> device,
                                     std::vector<unsigned char> *message) {
        DEBUG("Message received");
        printMsg(message);
        ConnectedDevice &connDev = defaultDevice; // Lookup and return if not found

        if ((*message)[0] == 0xE2 && (*message)[1] == 0x00 && connDev.state == 0) {
            DEBUG("Association Request");
            if (message->size() == 54 && (*message)[44] == 0x02 && (*message)[45] == 0xBC) {
                DEBUG("from BPM");
                unsigned char x[] = {

                    0xE3, 0x00,
                    0x00, 0x2C,
                    0x00, 0x00,
                    0x50, 0x79,
                    0x00, 0x26,
                    0x80, 0x00, 0x00, 0x00,
                    0x80, 0x00,
                    0x80, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00,
                    0x80, 0x00, 0x00, 0x00,
                    0x00, 0x08,
                    0x81, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                    0x00, 0x00,
                    0x00, 0x00,
                    0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00

                };
                std::vector<unsigned char> m(x,x+sizeof(x));
                device->sendMessagePrimary(&m);
                unsigned char y[] = {

                    0xE7, 0x00,
                    0x00, 0x0E,
                    0x00, 0x0C,
                    0x00, 0x24,
                    0x01, 0x03,
                    0x00, 0x06,
                    0x00, 0x00,
                    0x00, 0x00,
                    0x00, 0x00

                };
                std::vector<unsigned char> n(y,y+sizeof(y));
                device->sendMessagePrimary(&n);
                INFO("DIM SIGNAL: BPM connected");
                connDev.guid = "\"";
                connDev.guidval = 0;
                connDev.measurementQueue.clear();
                for (int i=36; i<44; i++) {
                    connDev.guidval <<= 8;
                    connDev.guidval |= (*message)[i];
                    connDev.guid += hexByte((*message)[i]);
                }
                connDev.guid += "\"";
                connDev.state = 1;
                connDev.sendNoMeasurement();
            }
        } else if ((*message)[0] == 0xE7 && (*message)[1] == 0x00 && message->size() == 172 && connDev.state == 1) {
            // Check guid value
            uint64_t gval = 0;
            for (int i=74; i<82; i++) {
                gval <<= 8;
                gval |= (*message)[i];
            }
            if (connDev.guidval != gval) {
                DEBUG("MDS Properties with GUID mismatch");
                return;
            }
            DEBUG("MDS properties");
            connDev.manufacturer = "\"";
            for (int i=42; i<54; i++) {
                connDev.manufacturer += ((*message)[i]);
            }
            connDev.manufacturer = QString(connDev.manufacturer.c_str()).trimmed().toStdString();
            connDev.manufacturer += "\"";
            connDev.model = "\"";
            for (int i=56; i<68; i++) {
                connDev.model += ((*message)[i]);
            }
            connDev.model = QString(connDev.model.c_str()).trimmed().toStdString();
            connDev.model += "\"";
            connDev.serialno = "\"";
            for (int i=114; i<126; i++) {
                connDev.serialno += ((*message)[i]);
            }
            connDev.serialno = QString(connDev.serialno.c_str()).trimmed().toStdString();
            connDev.serialno += "\"";

            connDev.phdevice = device;

            QDateTime deviceTime = QDateTime::fromString((timestamp(message,92)+"0").c_str(),"yyyy-MM-dd hh:mm:ss.zzz");
            cout << "Current device time: " << deviceTime.toString("yyyy-MM-dd hh:mm:ss").toStdString() << endl;
            connDev.timeDiff = deviceTime.secsTo(QDateTime::currentDateTime());
            if (connDev.timeDiff < -90 || connDev.timeDiff > 90) {
                // We will set the time
                unsigned char x[] = {

                    0xE7, 0x00,
                    0x00, 0x1A,
                    0x00, 0x18,
                    0x00, 0x77, // invoke id
                    0x01, 0x07,
                    0x00, 0x12,
                    0x00, 0x00,
                    0x0C, 0x17,
                    0x00, 0x0C,
                    0,0,0,0,  // Date
                    0,0,0,0,  // Time
                    0x00, 0x00, 0x00, 0x00

                };
                QString now = QDateTime::currentDateTime().toString("yyyyMMddhhmmsszzz");
                for (int i = 0; i < 8; i++) {
                    x[i+18] = (now[2*i].toLatin1() - '0') << 4 | (now[2*i+1].toLatin1() - '0');
                }
                std::vector<unsigned char> m(x,x+sizeof(x));
                cout << "Difference is: " << connDev.timeDiff << ", Setting time:" << endl;
                printMsg(&m);
                device->sendMessagePrimary(&m);

            } else {
                // Proceed to the connected state
                connDev.weAreConnected();
            }
        } else if ((*message)[0] == 0xE7 && (*message)[1] == 0x00 && (*message)[7] == 0x77 && message->size() == 18 && connDev.state == 1) {
            // Time set
            DEBUG("Time adjust completed");
            if (connDev.timeDiff < -300 || connDev.timeDiff > 300) {
                // If the time change exceeded 5 minutes, we will ignore any old measurements
                connDev.measurementQueue.clear();
            }
            // Proceed to the connected state
            connDev.weAreConnected();
        } else if ((*message)[0] == 0xE4 && (*message)[1] == 0x00 && message->size() == 6) {
            // Association release
            if (connDev.state == 2) { // Connected
                INFO("DIM SIGNAL: BPM disconnecting");
                connDev.state = 3;
                connDev.sendNoMeasurement();
            }
            unsigned char x[] = {

                0xE5, 0x00,
                0x00, 0x02,
                0x00, 0x00

            };
            std::vector<unsigned char> m(x,x+sizeof(x));
            device->sendMessagePrimary(&m);
            device->disconnect();
        } else if ((*message)[0] == 0xE6 && (*message)[1] == 0x00 && message->size() == 6) {
            // Abort connection
            if (connDev.state == 2) { // Connected
                INFO("DIM SIGNAL: BPM disconnecting");
                connDev.state = 3;
                connDev.sendNoMeasurement();
            }
            device->disconnect();
        } else if ((*message)[0] == 0xE7 && (*message)[1] == 0x00 && message->size() > 30
                   && (*message)[18] == 0x0D && (*message)[19] == 0x1D
                   && (u_int16_t)((*message)[28] * 256 + (*message)[29] + 30) == message->size()) {
            // Report
            u_int16_t invokeid = (*message)[6] * 256 + (*message)[7];
            u_int16_t reportCount = (*message)[26] * 256 + (*message)[27];
            int pointer = 30;
            for (int i = 0; i < reportCount; i++) {
                u_int16_t handle = (*message)[pointer + 0] * 256 + (*message)[pointer + 1];
                string json = "{ ";
                u_int16_t sys,dia,map,puls;
                switch (handle) {
                case 1:
                    if (!validTimestamp(message,pointer+14)) {
                        DEBUG("Old measurement dropped");
                        continue;
                    }
                    sys = (*message)[pointer + 8] * 256 + (*message)[pointer + 9];
                    dia = (*message)[pointer + 10] * 256 + (*message)[pointer + 11];
                    map = (*message)[pointer + 12] * 256 + (*message)[pointer + 13];
                    json += "\"type\" : 18948, \"datetime\" : \"" + timestamp(message, pointer+14);
                    json += "\", \"unit\" : 3872, \"values\" : [ { ";
                    json += "\"physio\" : 18949, \"value\" : " + sfloat(sys);
                    json += " }, { \"physio\" : 18950, \"value\" : " + sfloat(dia);
                    json += " }, { \"physio\" : 18951, \"value\" : " + sfloat(map);
                    pointer += 22;
                    break;
                case 2:
                    if (!validTimestamp(message,pointer+6)) {
                        DEBUG("Old measurement dropped");
                        continue;
                    }
                    puls = (*message)[pointer + 4] * 256 + (*message)[pointer + 5];
                    json += "\"type\" : 18474, \"datetime\" : \"" + timestamp(message, pointer+6);
                    json += "\", \"unit\" : 2720, \"values\" : [ { ";
                    json += "\"physio\" : null, \"value\" : " + sfloat(puls);
                    pointer += 14;
                    break;
                default:
                    throw std::runtime_error("Parsing problem");
                }
                json += "} ] }";
                if (connDev.state == 2) {
                    connDev.wrapMDS(json);
                } else {
                    connDev.measurementQueue.push_back(json);
                }
            }
            unsigned char x[] = {

                0xE7, 0x00,
                0x00, 0x12,
                0x00, 0x10,
                0,0,
                0x02, 0x01,
                0x00, 0x0A,
                0x00, 0x00,
                0x00, 0x00, 0x00, 0x00,
                0x0D, 0x1D,
                0x00, 0x00

            };
            x[6] = (invokeid >> 8) & 0xFF;
            x[7] = invokeid & 0xFF;
            std::vector<unsigned char> m(x,x+sizeof(x));
            device->sendMessagePrimary(&m);
        }
    }
};



class Wrapper
{
  PHDListener phdl;
  PAL::HDPWrapper hdp;

public:
  Wrapper()
    : hdp(phdl)
  {
    INFO("Wrapper constructing");
//    hdp.start();
//    hdp.registerDatatype(0x1007);
  }

  ~Wrapper()
  {
    INFO("Wrapper destruction");//

    defaultDevice.disassociate();
  }

};

SimpleDemoWrapper::SimpleDemoWrapper()
{
  pWrapper = new Wrapper();
}

SimpleDemoWrapper::~SimpleDemoWrapper()
{
  delete pWrapper;
}

