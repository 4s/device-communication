#ifndef CORE_H
#define CORE_H

#include "PAL/Interface/basemsg.h"

namespace fsdc
{

/**
 * @brief 4SDC Core class
 *
 * The core class of 4SDC, that ensures the rest of 4SDC is started so it can
 * respond to requests on its interface.
 *
 * As part of initialisation, the core class will spawn a seperate thread for
 * 4SDC to run in.
 *
 * To Initialise the system, you must from somewhere in you program call
 * Core::start()  - to terminate the system call Core::terminate(), both
 * functions are static, so you don't need an instance of the object to do so.
 *
 * When the core is started up it will emit the Core::Ready signal
 */
class Core
{

public:
  Core();

  /**
   * @brief Calling start will begin initialization of the 4SDC module
   *
   * This function will spawn a thread and start the 4SDC module, each call
   * to start must be followed by a call to terminat at a later point in time.
   *
   * (The numbers of calls to start must match the number of calls to terminate)
   *
   * After 4SDC has completed starting it will broadcast a Core::Ready signal,
   * at which point it will be possible to send signals to 4SDC.
   *
   * Sending signals to 4SDC before the Core::Ready signal has been received
   * will cause undefined behvaiour.
   *
   * If you call start after terminate has been initiated, the module will
   * restart it self after the current termination has completed, if at that
   * time the start call(s) havn't been cancled by an equal numbers of terminate
   * calls.
   *
   * @todo The optional wait parameter can be set to true to ensure the function
   * doesn't return until 4SDC has started.
   */
  static void start( bool wait=false );

  /**
   * @brief Terminates the 4SDC module
   *
   * Calling this function will start the shutdown sequence of the module, it is
   * legal to call terminate before Core::Ready has been sent, but it is not
   * guranteed that any signals sent before the call won't be handled.
   *
   * When termination has completed, a Core::Terminated signal will be
   * broadcasted.
   *
   * @note It will cause undefined behavior to send any signals from the call to
   * terminate has been made until a Core::Terminated signal has been received.
   *
   * @todo The optional wait parameter can be set to true to ensure the function
   * doesn't return until termination has completed. But be carefull, in the
   * case that multiple calls to start was made, the function will not return
   * until the same number of calls to terminate has been made (this might
   * happen from other threads).
   */
  static void terminate( bool wait=false );

  /**
   * @brief Ready signal
   *
   * The Core::Ready signal is sent when the 4SDC module is up and running
   */
  class Ready : public FSYS::BaseMsg {};

  /**
   * @brief Terminated signal
   *
   * The Core::Terminated signal is sent when the 4SDC module has shut down
   * freed all of its resources and its thread is about to terminate
   */
  class Terminated : public FSYS::BaseMsg {};

  /**
   * @brief Terminating signal
   *
   * This signal is send when the system is about to terminate the msg loop for
   * the core, it is sent to give all entities a chance to clean up.
   *
   *
   */
  class Terminating : public FSYS::BaseMsg {};
};

} // End namespace


#endif // CORE_H

