#include "core.h"


#include "msgqueue.h"
#include "msgsender.h"
#include "msgreceiver.h"
#include "simpledemowrapper.h"

#include <cassert>
#include <atomic>
#include <mutex>
#include <thread>

namespace
{

class MainThread : public FSYS::MsgSender<MainThread>, public FSYS::MsgReceiver<MainThread, fsdc::Core::Terminated>
{
  friend class fsdc::Core;
  std::mutex runningStateMutex;

  std::thread *fsdcThread;

  enum RunningState {
    NOT_RUNNING,
    STARTING,
    RUNNING,
    TERMINATING
  } state;


  // Contains the number of starts that are called without a corresponding stop
  int excessStartsRequested;

  void requestStart()
  {
    // Make sure we have full control over the counters
    std::lock_guard<std::mutex> lock (runningStateMutex);

    // If this is not the first one, then we don't do anything
    if(excessStartsRequested++) return;

    // Otherwise it must be the first one, in which case we only do anything if
    // we are in the "not running" state
    if(state != NOT_RUNNING) return;

    // Call function to start it
    doStart();
  }

  /**
   * @brief Function to start the worker thread_local
   *
   * When calling this function the runningStatMutex must have been already
   * taken.
   *
   */
  void doStart()
  {
    // Change state
    state = STARTING;

    // Spawn the thread
    fsdcThread = new std::thread(MainThread::runStatic, this);
  }

  void requestEnd()
  {
    // Make sure we have full control over the counters
    std::lock_guard<std::mutex> lock (runningStateMutex);

    // Sanity check our counter
    assert(excessStartsRequested > 0);

    // Decrease the counter
    excessStartsRequested--;

    // Unless the counter has reached 0, we don't do anything
    if(excessStartsRequested > 0) return;

    // Change state and broadcast that we are terminating
    // (The terminating signal will be cought by the thread, and make it shut
    // it self down)
    state = TERMINATING;

    fsdc::Core::Terminating terminating;
    broadcast(terminating);
  }

public:
  // Function called when the thread has send it's terminated signal
  void receive(fsdc::Core::Terminated &)
  {
    // Make sure no-one tries to start us while we are shutting down
    std::lock_guard<std::mutex> lock (runningStateMutex);

    // Wait for the thread to end
    fsdcThread->join();

    // Delete the object
    delete fsdcThread;
    fsdcThread = nullptr;

    // Check if we should restart, or remain not running
    if(0 < excessStartsRequested)
    {
      // Do the magic to change state and start the thread
      doStart();
      return;
    }

    // We are done shutting down
    state = NOT_RUNNING;

  }
public:
  explicit MainThread(void)
    : fsdcThread(nullptr),
      state(NOT_RUNNING),
      excessStartsRequested(0)
  {
  }

  ~MainThread(void)
  {
    if(nullptr != fsdcThread)
    {
      delete fsdcThread;
      fsdcThread = nullptr;
    }
  }
private:
  /**
   * @brief Static run function, used for starting the object run function
   *
   * @param thiz the this pointer for the object run is called with
   */
  static void runStatic(MainThread *thiz)
  {
    // Call the correct object
    thiz->run();
  }

  /**
   * @brief The main run function excecuted in its own thread
   *
   */
  void run()
  {
    // We need to have this class instantiated inside the run function so it
    // is initialized in the context of the thread where it must run
    class Terminator : public FSYS::MsgReceiver<Terminator, fsdc::Core::Terminating>
    {
    public:
      bool bTerminating;
      Terminator():bTerminating(false){}
      void receive(fsdc::Core::Terminating &)
      {
        bTerminating=true;
        FSYS::MsgQueue::breakEmptyMsgQueue();
      }
    } terminator;
    // While we were starting up and the Terminator object being created, we
    // could have missed a terminating signal, so we need to check the state
    {
      std::lock_guard<std::mutex> lock (runningStateMutex);
      if(STARTING != state)
      {
        terminator.bTerminating=true;
      }
    }
    // We want to limit the scope of the wrapper object
    {
      // Initialiser
      SimpleDemoWrapper demoWrapper;

      fsdc::Core::Ready ready;
      broadcast(ready);

      // Loop as long as we havn't been asked to terminate
      while(!terminator.bTerminating)
      {
        // Wait for the msg queue to contain data
        FSYS::MsgQueue::waitUntilQueueHasData();

        // Empty the msg queue
        FSYS::MsgQueue::emptyMsgQueue();
      }
    }
    fsdc::Core::Terminated terminated;
    broadcast(terminated);
  }
} mainThread;

} // End namespace fsdc

void fsdc::Core::start( bool )
{
  mainThread.requestStart();
}

void fsdc::Core::terminate( bool )
{
  mainThread.requestEnd();
}
