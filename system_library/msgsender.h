#ifndef MSGSENDER_H
#define MSGSENDER_H

#include "msgsenderbase.h"

#include <memory>


namespace FSYS
{
  template<class parentClass> class MsgSender : private MsgSenderBase
  {
  private:
    void* getSenderMagicKey( void )
    {
      return static_cast<parentClass*>(this);
    }

  public:
    template<class T> void broadcast(T &msg)
    {
      std::shared_ptr<BaseMsg> pMsg(new T(msg));
      MsgSenderBase::broadcast(pMsg, typeid(T));
    }

    template<class T> void respond(T &msg, BaseMsg &received)
    {
      std::shared_ptr<BaseMsg> pMsg(new T(msg));
      MsgSenderBase::respond(pMsg, typeid(T), received);
    }

  };
}
#endif // MSGSENDER_H
