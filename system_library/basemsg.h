#ifndef BASEMSG_H
#define BASEMSG_H

#include "handle.h"

namespace FSYS
{


class MsgQueue;
class MsgSenderBase;

class BaseMsg
{
  friend class MsgQueue;
  friend class MsgSenderBase;

  Handle originObject;
  Handle originQueue;
  void * originMagicKey;

  void clearOrigin( void );

public:
  BaseMsg( void )
    : originObject(Handle::null()),
      originQueue(Handle::null()),
      originMagicKey(nullptr)
  {
  }
};
}

#endif // BASEMSG_H
