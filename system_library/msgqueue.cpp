
#include"Interface/msgqueue.h"
#include"msgqueuebase.h"

void FSYS::MsgQueue::addListenerToQueue(void *magicPointer,
                               const std::type_info &typeOfMsg,
                               MsgCallBack *msgCallBack)
{
  FSYS::MsgQueueBase::addListenerToQueue(magicPointer,
                                         typeOfMsg,
                                         msgCallBack);
}

void FSYS::MsgQueue::removeListenerFromQueue(MsgCallBack *msgCallBack)
{
  FSYS::MsgQueueBase::removeListenerFromQueue(msgCallBack);
}

void FSYS::MsgQueue::waitUntilQueueHasData( void )
{
  FSYS::MsgQueueBase::waitUntilQueueHasData();
}

void FSYS::MsgQueue::emptyMsgQueue( int maxTimeMs )
{
  FSYS::MsgQueueBase::emptyMsgQueue(maxTimeMs);
}

void FSYS::MsgQueue::breakEmptyMsgQueue( void )
{
  FSYS::MsgQueueBase::breakEmptyMsgQueue();
}

bool FSYS::MsgQueue::isEmpty( void )
{
  return FSYS::MsgQueueBase::isEmpty();
}
