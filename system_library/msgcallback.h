#ifndef MSGCALLBACK_H
#define MSGCALLBACK_H

#include "basemsg.h"
#include "handle.h"

#include <memory>


namespace FSYS
{
  class MsgCallBack : virtual public Handle
  {
  public:
    MsgCallBack( void ) {}

    virtual void callBack(std::shared_ptr<BaseMsg> &baseMsg) = 0;
  };

  template<class T> class MsgCallBackT : public MsgCallBack
  {
  public:
    virtual void callBack(T &arg) = 0;

    void callBack(std::shared_ptr<BaseMsg> &baseMsg)
    {
      T &msg = static_cast<T&>(*baseMsg);
      callBack(msg);
    }
  };

}

#endif // MSGCALLBACK_H
