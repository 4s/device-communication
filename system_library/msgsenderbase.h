#ifndef MSGSENDERBASE_H
#define MSGSENDERBASE_H

#include "handle.h"

#include <memory>

namespace FSYS
{

  class BaseMsg;

  class MsgSenderBase : virtual Handle
  {
  public:
    std::shared_ptr<BaseMsg> &fillOutSenderInfo(std::shared_ptr<BaseMsg> &msg);
    void broadcast(std::shared_ptr<BaseMsg> &msg, const std::type_info &typeOfMsg);
    void respond(std::shared_ptr<BaseMsg> &msg,
                 const std::type_info &typeOfMsg,
                 BaseMsg &received);

    virtual void* getSenderMagicKey( void )  = 0;
  };
}

#endif // MSGSENDERBASE_H
