#include "Interface/msgsenderbase.h"

#include "Interface/basemsg.h"
#include "Interface/msgqueue.h"
#include "msgqueuebase.h"

#include <assert.h>


std::shared_ptr<FSYS::BaseMsg> &FSYS::MsgSenderBase::fillOutSenderInfo(std::shared_ptr<BaseMsg> &msg)
{
  // Make sure this message hasn't been sent before
  assert(msg->originQueue == Handle::null());

  // Fill out all the information we know about our self
  msg->originQueue = MsgQueueBase::getThreadQueue();
  msg->originMagicKey = getSenderMagicKey();
  msg->originObject = *this;

  return msg;
}

void FSYS::MsgSenderBase::broadcast(std::shared_ptr<BaseMsg> &msg, const std::type_info &typeOfMsg)
{
  // Broadcast the message
  MsgQueueBase::sendMsg(fillOutSenderInfo(msg),         // Msg to send
                        typeOfMsg,                      // Type of msg being sent
                        MsgQueueBase::getBroadCastHandle(), // Send to all queues
                        MsgQueueBase::getBroadCastHandle(), // Send to all objects
                        nullptr                         // Spam it
                       );
}

void FSYS::MsgSenderBase::respond(std::shared_ptr<BaseMsg> &msg,
                                  const std::type_info &typeOfMsg,
                                  BaseMsg &received)
{
  // Send the message to specific
  MsgQueueBase::sendMsg(fillOutSenderInfo(msg),  // Msg to send
                        typeOfMsg,               // Type of msg being sent
                        received.originQueue,    // Msg queue to target
                        received.originObject,   // Msg object to target
                        received.originMagicKey  // MagicKey to target
                        );
}



