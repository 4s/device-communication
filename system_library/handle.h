#ifndef HANDLE_H
#define HANDLE_H

#include <cstdint>

namespace FSYS
{

  class Handle
  {
    int64_t handle;
    int64_t getNextHandle( void );
  public:
    Handle( void ) {handle = getNextHandle();}
    Handle( Handle &ref ) : handle(ref.handle) {}
    Handle( const Handle &ref ) : handle(ref.handle) {}

    bool operator==(Handle const &compareTo) const {return (this->handle == compareTo.handle);}
    bool operator!=(Handle const &compareTo) const {return (this->handle != compareTo.handle);}
    bool operator>(Handle const &compareTo) const {return (this->handle > compareTo.handle);}
    bool operator<(Handle const &compareTo) const {return (this->handle < compareTo.handle);}

    static Handle &broadCast( void );
    static Handle &null( void );
  };
}

#endif // HANDLE_H
