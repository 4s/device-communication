#ifndef MSGRECEIVER_H
#define MSGRECEIVER_H

#include "msgcallback.h"
#include "msgqueue.h"

namespace FSYS
{
  template<class TReceiverClass, class TMsg> class MsgReceiver : public MsgCallBackT<TMsg>
  {
  public:
    MsgReceiver( void )
    {
      // Register this object on the listener queue of this thread
      MsgQueue::addListenerToQueue(static_cast<void *>(parentClassPointer()),
                                   typeid(TMsg),
                                   this);
    }

    ~MsgReceiver()
    {
      // Remove this object for the listener queue
      MsgQueue::removeListenerFromQueue(this);
    }

    TReceiverClass* parentClassPointer()
    {
      return static_cast<TReceiverClass*>(this);
    }

    void callBack(TMsg &msg)
    {
      parentClassPointer()->receive(msg);
    }
  };
}

#endif // MSGRECEIVER_H
