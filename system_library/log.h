#ifndef LOG_H
#define LOG_H

#include <string>

namespace FSYS
{
  /**
     * @brief Log class used for logging information about program execution
     *
     * You don't need to instantiate a log object to use the system, you can
     * if you want to, but it is recommended that you use the macro system
     * instead.
     *
     * To just get a log line out:
     *
     * @code
     *  #include "PAL/Interface/log.h"
     *
     *  ...
     *  FSYS::Log::info("This is an info message");
     *  ...
     * @endcode
     *
     * The above will output something like:
     *
     * Info: "This is an info message"
     *
     * How ever, it is recomended to use the macro interface:
     *
     * @code
     * #include "PAL/Interface/log.h"
     *
     *
     * DECLARE_LOG_MODULE("MyModuleName");
     *
     * ...
     *
     * INFO("This is an info message");
     *
     * ...
     * @endcode
     *
     * As this will also output the modulename, filename, and line number:
     *
     * Info: (MyModuleName)"This is an info message"\@filepath(line number)
     *
     * Other macros are:
     *
     *  - FATAL(msg) - For fatal, non-recoverable errors (leads to program
     * crash/closure)
     *
     *  - ERR(msg) - For error messages, that are handled in one way or the
     * other
     *
     *  - WARN(msg) - For warnings/unexpected behavior
     *
     *  - DEBUG(msg) - For debug messages, used during development
     *
     *  - INFO(msg) - For info/convinient messages usefull for knowing the state
     * of the program
     *
     * @note It is not possible to have multiple calls to DECLARE_LOG_MODULE in
     * the same source file.
     */
    class Log
    {
    protected:
      /**
       * @brief The me member is available for use for actuall implementations
       *
       * The me member is not part of the public interface of the Log class, it
       * has been added in case any of the platform specific implementations
       * of the Log class needs to store some kind of information in the object.
       */
      void *me;
    public:

      /**
       * @brief The Log constructor
       *
       * The Log constructor takes a module name in, the form of a string, as
       * argument, this string will be appended to all of the non-static log
       * strings in the interface.
       *
       * The same module name can be used across multiple source files, and it
       * is possible to have multiple log objects/module names in the same
       * source file (as long as you don't use the macros).
       *
       * You don't need to instantiate the class if you only use the static
       * functions in it.
       *
       * @param module The
       */
      Log(std::string const &module);
      ~Log();

      /**
       * @brief Static function to log messages about fatal incidents in the code
       *
       * @code
       *  #include "PAL/Interface/log.h"
       *
       *  ...
       *  FSYS::Log::fatal("This is a message about a fatal event");
       *  ...
       * @endcode
       *
       * @param fatal String reference to the message that should be logged as
       *              fatal
       */
      static void fatal(std::string const &fatal);

      /**
       * @brief Static function to log messages about error conditions in the code
       *
       * @code
       *  #include "PAL/Interface/log.h"
       *
       *  ...
       *  FSYS::Log::err("This is a message about an error");
       *  ...
       * @endcode
       *
       * @param err String reference to the error message that should be logged
       */
      static void err  (std::string const &err);

      /**
       * @brief Static function to log warning messages from the code
       *
       * @code
       *  #include "PAL/Interface/log.h"
       *
       *  ...
       *  FSYS::Log::warn("This is a warning message");
       *  ...
       * @endcode
       *
       * @param warn String reference to the message that should be logged as a
       *             warning
       */
      static void warn (std::string const &warn);

      /**
       * @brief Static function to log debug messages
       *
       * @code
       *  #include "PAL/Interface/log.h"
       *
       *  ...
       *  FSYS::Log::debug("This is a debug message");
       *  ...
       * @endcode
       *
       * @param debug String reference to the debug message
       */
      static void debug(std::string const &debug);

      /**
       * @brief Static function to log info messages
       *
       * @code
       *  #include "PAL/Interface/log.h"
       *
       *  ...
       *  FSYS::Log::debug("This is an info message");
       *  ...
       * @endcode
       *
       * @param debug String reference to the info message
       */
      static void info (std::string const &info);

      void fatal(std::string const &fatal, std::string const &file, int line);
      void err  (std::string const &err,   std::string const &file, int line);
      void warn (std::string const &warn,  std::string const &file, int line);
      void debug(std::string const &debug, std::string const &file, int line);
      void info (std::string const &info,  std::string const &file, int line);

#define DECLARE_LOG_MODULE(module) namespace {FSYS::Log eRRORlOG(module);}

#define FATAL(msg) eRRORlOG.fatal(msg, __FILE__, __LINE__);
#define ERR(msg)   eRRORlOG.err  (msg, __FILE__, __LINE__);
#define WARN(msg)  eRRORlOG.warn (msg, __FILE__, __LINE__);
#define DEBUG(msg) eRRORlOG.debug(msg, __FILE__, __LINE__);
#define INFO(msg)  eRRORlOG.info (msg, __FILE__, __LINE__);

    };

}

#endif // LOG_H
