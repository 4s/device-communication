#include "Interface/handle.h"

#include <cassert>
#include <mutex>


int64_t FSYS::Handle::getNextHandle( void )
{
  static std::mutex mutex;
  static int64_t counter = 0;
  std::lock_guard<std::mutex> guard(mutex);

  assert(counter >= 0);

  return ++counter;
}

FSYS::Handle &FSYS::Handle::broadCast( void )
{
  // Constructor will initialise handle to have unique value
  static Handle broadCastHandle;

  return broadCastHandle;
}

FSYS::Handle &FSYS::Handle::null( void )
{
  // Constructor will initialise handle to have unique value
  static Handle broadCastHandle;

  return broadCastHandle;
}

