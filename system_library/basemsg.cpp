#include "Interface/basemsg.h"

void FSYS::BaseMsg::clearOrigin( void )
{
  originObject = Handle::null();
  originQueue = Handle::null();
  originMagicKey = nullptr;
}
