#ifndef MSGQUEUE_H
#define MSGQUEUE_H

#include <typeinfo>

namespace FSYS
{
  class MsgCallBack;

  class MsgQueue
  {
  public:

    static void addListenerToQueue(void *magicPointer,
                                   const std::type_info &typeOfMsg,
                                   MsgCallBack *msgCallBack);

    static void removeListenerFromQueue(MsgCallBack *msgCallBack);



    static void waitUntilQueueHasData( void );

    /**
     * @brief The function will process messages on the message queue
     *
     * This function is used for processing messages from the message queue
     * of the thread that this function is called from.
     *
     * The default behavior is for the function to run until there are no
     * more messages on the queue at which point the function will return.
     *
     * It is possible to give an optional argument to the function limiting
     * the time it will run.
     *
     * @param maxTimeMs  The maximum time in MS that will pass before the
     *                   function breaks the loop that empties the queue.
     *
     *                   maxTimeMs <  0 => Run until the queue is empty or
     *                                     breakEmptyMsgQueue() is called
     *
     *                   maxTimeMS == 0 => Run until all messages on the queue
     *                                     at the time of the call are processed
     *
     *                   maxTimeMS >  0 => Run approximately until maxTimeMS
     *                                     have passed
     */
    static void emptyMsgQueue( int maxTimeMs=-1 );

    /**
     * @brief Breaks out of emptyMsgQueue
     *
     * Function must be called from a message handler (directly or indirectly)
     * and will cause the message loop internal to the emptyMsgQueue function to
     * break.
     *
     * This function is used in the case where you have a component that as part
     * of its operation sends messages to it self.
     *
     * Usually the emptyMsgQueue function will only return when the message is
     * queue is empty, but in the case where components on the queue keeps
     * sending to other components in the same thread, the queue will never be
     * empty and hence will never terminate.
     *
     * Calling this function will break the msg loop.
     */
    static void breakEmptyMsgQueue( void );

    /**
     * @brief Function to determine if there is data on the msg queue
     *
     * Call this function to determine if there are messages waiting to be
     * processed on the queue.
     *
     * @return true if there is messages waiting to be processd, false if not
     */
    static bool isEmpty( void );
  };
}
#endif // MSGQUEUE_H
