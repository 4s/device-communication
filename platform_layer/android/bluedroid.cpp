#include "bluedroid.h"
#include "Interface/log.h"

#include <memory>

DECLARE_LOG_MODULE("Bluedroid")


HDPConnector *singleton;

void connectIndication(JNIEnv *env, jobject thiz, jint device) {
    (void)env;
    (void)thiz;
    int dev = (int) device;
    if (singleton) {
        singleton->handleConnectIndication(dev);
    }
}

void disconnectIndication(JNIEnv *env, jobject thiz, jint device) {
    (void)env;
    (void)thiz;
    int dev = (int) device;
    if (singleton) {
        singleton->handleDisconnectIndication(dev);
    }
}

void messageReceived(JNIEnv *env, jobject thiz, jint device, jbyteArray message) {
    (void)thiz;
    int dev = (int) device;
    int len(env->GetArrayLength(message));
    int8_t *msg(env->GetByteArrayElements(message,0));
    if (singleton) {
        singleton->handleMessageReceived(dev,len,msg);
    }
}


class HDP;

class HealthDevice {
private:
    HDP *hdp;
    std::shared_ptr<PersonalHealthDevice> phddevice;
    int deviceNumber;

public:
    HealthDevice(int deviceNumber);
    ~HealthDevice();
    QString getBTAddress();
    QString getName();
    void sendMessagePrimary(std::vector<unsigned char> *message);
    void sendMessageStreaming(std::vector<unsigned char> *message);
    void disconnect(bool force);
    void connectIndication();
    void disconnectIndication();
    void messageReceived(int len, int8_t *buffer);
};


class HDP : public HDPDevice {
private:
    bool connected;
    HealthDevice *parent;
public:
    bool isConnected() {return connected;}

    void sendMessagePrimary(std::vector<unsigned char> *message) {
        if (parent) parent->sendMessagePrimary(message);
    }

    void sendMessageStreaming(std::vector<unsigned char> *message) {
        if (parent) parent->sendMessageStreaming(message);
    }

    std::string getBTAddress() {
        if (parent) return parent->getBTAddress().toStdString();
        return "";
    }

    std::string getName() {
        if (parent) return parent->getName().toStdString();
        return "";
    }

    void disconnect() {
        // FIXME: Dette vil med det samme sende et
        // PersonalHealthDeviceListener::disconnectIndication() signal,
        // hvilket bryder med den sekventielle ordning af beskeder...
        // Problemet vil måske blive løst af kø-systemet.
        if (parent) parent->disconnect(true);
    }

    HDP() : connected(false), parent(NULL) {}
    HDP(HealthDevice *parent) : connected(true), parent(parent) {}

    void kill() { connected = false; parent = NULL; }

};



HealthDevice::HealthDevice(int deviceNumber) :
    hdp(NULL),
    phddevice(new HDP()),
    deviceNumber(deviceNumber) {


}

HealthDevice::~HealthDevice() {
    disconnect(true);
}

QString HealthDevice::getBTAddress() {
    jint d(deviceNumber);
    QAndroidJniObject retval = QAndroidJniObject::callStaticObjectMethod("dk/fs/fsdc/framework/HDPAdaptor",
                       "getBTAddress", "(I)Ljava/lang/String;", d);
    QAndroidJniEnvironment env;
    if (env->ExceptionCheck()) {
        // FIXME: No exception handling
        ERR("FIXME: Exception in Java NOT CAUGHT!!")
    }
    return retval.toString();
}

QString HealthDevice::getName() {
    jint d(deviceNumber);
    QAndroidJniObject retval = QAndroidJniObject::callStaticObjectMethod("dk/fs/fsdc/framework/HDPAdaptor",
                       "getName", "(I)Ljava/lang/String;", d);
    QAndroidJniEnvironment env;
    if (env->ExceptionCheck()) {
        // FIXME: No exception handling
        ERR("FIXME: Exception in Java NOT CAUGHT!!")
    }
    return retval.toString();
}

void HealthDevice::sendMessagePrimary(std::vector<unsigned char> *message) {
    QAndroidJniEnvironment env;
    jbyteArray argument = env->NewByteArray(message->size());
    env->SetByteArrayRegion(argument,0,message->size(),(jbyte*)message->data());

    jint d(deviceNumber);
    QAndroidJniObject::callStaticMethod<void>("dk/fs/fsdc/framework/HDPAdaptor",
                       "sendMessagePrimary", "(I[B)V", d, argument);
    if (env->ExceptionCheck()) {
        // FIXME: No exception handling
        ERR("FIXME: Exception in Java NOT CAUGHT!!")
    }

    env->DeleteLocalRef(argument);
}

void HealthDevice::sendMessageStreaming(std::vector<unsigned char> *message) {
    QAndroidJniEnvironment env;
    jbyteArray argument = env->NewByteArray(message->size());
    env->SetByteArrayRegion(argument,0,message->size(),(jbyte*)message->data());

    jint d(deviceNumber);
    QAndroidJniObject::callStaticMethod<void>("dk/fs/fsdc/framework/HDPAdaptor",
                       "sendMessageStreaming", "(I[B)V", d, argument);
    if (env->ExceptionCheck()) {
        // FIXME: No exception handling
        ERR("FIXME: Exception in Java NOT CAUGHT!!")
    }

    env->DeleteLocalRef(argument);
}

void HealthDevice::disconnect(bool force) {
    if (force) {
        jint d(deviceNumber);
        QAndroidJniObject::callStaticMethod<void>("dk/fs/fsdc/framework/HDPAdaptor",
                           "disconnect", "(I)V", d);
        QAndroidJniEnvironment env;
        if (env->ExceptionCheck()) {
            // FIXME: No exception handling
            ERR("FIXME: Exception in Java NOT CAUGHT!!")
        }
    }
    if (hdp) {
        hdp->kill();
        hdp = NULL;
        singleton->listener.disconnectIndication(phddevice);
    }
}

void HealthDevice::connectIndication() {
    if (!hdp) {
        // New connection
        hdp = new HDP(this);
        phddevice = std::shared_ptr<PersonalHealthDevice>(hdp);
        singleton->listener.connectIndication(phddevice);
    }
}

void HealthDevice::disconnectIndication() {
    disconnect(false);
}

void HealthDevice::messageReceived(int len, int8_t *buffer) {
    if (hdp) {
        std::vector<unsigned char> message;
        message.reserve(len);
        for (int i=0; i<len; i++) message.push_back((uint8_t)buffer[i]);
        singleton->listener.messageReceived(phddevice, &message);
    }
}


HDPConnector::HDPConnector(PersonalHealthDeviceListener &listener) :
        listener(listener) {

    if (singleton) {
        throw ConnectionException("HDPConnector must only be instantiated once");
    }
    singleton = this;

    // Register native/c++ function to handle call from Java
    static const JNINativeMethod methods[] = {{"connectIndication",
                                               "(I)V",
                                               reinterpret_cast<void *>(connectIndication)},
                                              {"disconnectIndication",
                                               "(I)V",
                                               reinterpret_cast<void *>(disconnectIndication)},
                                              {"messageReceived",
                                               "(I[B)V",
                                               reinterpret_cast<void *>(messageReceived)},
                                             };

    QAndroidJniObject javaClass("dk/fs/fsdc/framework/HDPAdaptor");
    QAndroidJniEnvironment env;
    jclass objectClass = env->GetObjectClass(javaClass.object<jobject>());
    jint result = env->RegisterNatives(objectClass,
                         methods, sizeof(methods) / sizeof(methods[0]));
    env->DeleteLocalRef(objectClass);

    if (result) {
        ERR("Error registering native methods");
    }
}

HDPConnector::~HDPConnector() {
    foreach (HealthDevice *h, devices.values()) {
        if (h) delete h;
    }
    devices.clear();
}

void HDPConnector::receive(fsdc::Core::Ready)
//void HDPConnector::start()
{

    QAndroidJniObject::callStaticMethod<void>("dk/fs/fsdc/framework/HDPAdaptor",
                                               "start", "()V");
    QAndroidJniEnvironment env;
    if (env->ExceptionCheck()) {
        // FIXME: No exception handling
        ERR("FIXME: Exception in Java NOT CAUGHT!!")
    }
}

void HDPConnector::receive(HDPWrapper::RegisterDataType &signal)
//void HDPConnector::registerDatatype(unsigned dataType)
{

    jint dt(signal.dataType);
    QAndroidJniObject::callStaticMethod<void>("dk/fs/fsdc/framework/HDPAdaptor",
                                              "registerDatatype", "(I)V", dt);
    QAndroidJniEnvironment env;
    if (env->ExceptionCheck()) {
        // FIXME: No exception handling
        ERR("FIXME: Exception in Java NOT CAUGHT!!")
    }
}

void HDPConnector::receive(HDPWrapper::UnregisterDataType &signal)
//void HDPConnector::unregisterDatatype(unsigned dataType)
{
    jint dt(signal.dataType);
    QAndroidJniObject::callStaticMethod<void>("dk/fs/fsdc/framework/HDPAdaptor",
                                              "unregisterDatatype", "(I)V", dt);
    QAndroidJniEnvironment env;
    if (env->ExceptionCheck()) {
        // FIXME: No exception handling
        ERR("FIXME: Exception in Java NOT CAUGHT!!")
    }
}

void HDPConnector::handleConnectIndication(int device) {
    if (devices.contains(device)) {
        devices[device]->connectIndication();
    } else {
        HealthDevice *h = new HealthDevice(device);
        devices.insert(device,h);
        h->connectIndication();
    }
}

void HDPConnector::handleDisconnectIndication(int device) {
    if (devices.contains(device)) {
        devices[device]->disconnectIndication();
    }
}

void HDPConnector::handleMessageReceived(int device, int len, int8_t *buffer) {
    if (devices.contains(device)) {
        devices[device]->messageReceived(len,buffer);
    }
}





HDPWrapper::HDPWrapper(PersonalHealthDeviceListener &listener) :
    connector(new HDPConnector(listener)) {}

HDPWrapper::~HDPWrapper() {
    delete connector;
}

