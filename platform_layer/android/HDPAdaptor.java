package dk.fs.fsdc.framework;

import android.util.Log;
import android.content.Context;
import android.os.ParcelFileDescriptor;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHealth;
import android.bluetooth.BluetoothHealthAppConfiguration;
import android.bluetooth.BluetoothHealthCallback;
import android.bluetooth.BluetoothProfile;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


public class HDPAdaptor {

    private static final String TAG = "HDPAdaptor";

    private static BluetoothAdapter bluetoothAdapter;
    private static BluetoothHealth bluetoothHealth = null;
    private static int lastDeviceID = 0;

    private static Context context = null;
    private static FileOutputStream wr;

    public static void setContext(Context c) {
        context = c;
    }


    private static final BluetoothProfile.ServiceListener btServiceListener
        = new BluetoothProfile.ServiceListener() {
            public void onServiceConnected(int profile, BluetoothProfile proxy) {
                if (profile == BluetoothProfile.HEALTH) {
                    bluetoothHealth = (BluetoothHealth) proxy;
                    Log.w(TAG, "onServiceConnected to profile HEALTH");
                    // FIXME: Remove me!
                    bluetoothHealth.registerSinkAppConfiguration("FIXME:GetADecentName", 4103, healthCallback);
                }
            }

            public void onServiceDisconnected(int profile) {
                if (profile == BluetoothProfile.HEALTH) {
                    bluetoothHealth = null;
                }
            }
        };

    private static final BluetoothHealthCallback healthCallback
        = new BluetoothHealthCallback() {
            public void onHealthAppConfigurationStatusChange(BluetoothHealthAppConfiguration config,
                                                             int status) {
                if (status == BluetoothHealth.APP_CONFIG_REGISTRATION_FAILURE) {
                    Log.e(TAG,"App Config registration failed");
                    // FIXME: Report the error?
                } else if (status == BluetoothHealth.APP_CONFIG_REGISTRATION_SUCCESS) {
                    Log.d(TAG,"App Config registration success");
                    // Notice: may be called multiple times...
                    //configs.add(config);
                    //sendMessage(STATUS_HEALTH_APP_REG, RESULT_OK, null);
                } else if (status == BluetoothHealth.APP_CONFIG_UNREGISTRATION_FAILURE ||
                            status == BluetoothHealth.APP_CONFIG_UNREGISTRATION_SUCCESS) {
                    // Currently ignored...
                }
            }

            public void onHealthChannelStateChange(BluetoothHealthAppConfiguration config,
                            BluetoothDevice device, int prevState, int newState, ParcelFileDescriptor fd,
                            int channelId) {
                if (prevState == BluetoothHealth.STATE_CHANNEL_DISCONNECTED &&
                                newState == BluetoothHealth.STATE_CHANNEL_CONNECTED) {
                    Log.d(TAG,"Channel connected");
//                    if (acceptsConfiguration(config)) {
//                        insertDeviceConfiguration(device, config);
//                        insertChannelId(device, channelId);
                        connectIndication(1);
                        wr = new FileOutputStream(fd.getFileDescriptor());
//                        insertWriter(device, wr);
                        (new ReaderThread(1, fd)).start();
//                    } else {
//                        sendMessage(STATUS_CREATE_CHANNEL, RESULT_FAIL, device);
//                    }
                } else if (prevState == BluetoothHealth.STATE_CHANNEL_CONNECTING &&
                            newState == BluetoothHealth.STATE_CHANNEL_DISCONNECTED) {
//                    sendMessage(STATUS_CREATE_CHANNEL, RESULT_FAIL, device);
//                    removeWriter(device);
                } else if (newState == BluetoothHealth.STATE_CHANNEL_DISCONNECTED) {
                      disconnectIndication(1);
                      wr = null;
/*                    if (acceptsConfiguration(config)) {
                        sendMessage(STATUS_DESTROY_CHANNEL, RESULT_OK, device);
                        removeWriter(device);
                    } else {
                        sendMessage(STATUS_DESTROY_CHANNEL, RESULT_FAIL, device);
                        removeWriter(device);
                    }*/
                }
            }
        };



    private static class ReaderThread extends Thread {
        int device;
        ParcelFileDescriptor fd;
        public ReaderThread(int device, ParcelFileDescriptor fd) {
            super();
            this.device = device;
            this.fd = fd;
        }

        @Override
        public void run() {
            FileInputStream is = new FileInputStream(fd.getFileDescriptor());
            final byte[] data = new byte[63*1024];
            try {
                int len;
                while ((len = is.read(data)) > -1) {
                    if (len > 0) {
                        byte[] buf = new byte[len];
                        System.arraycopy(data, 0, buf, 0, len);
                        messageReceived(device, buf);
                    }
                }
            } catch (IOException e) {
                // FIXME: Currently ignoring all problems
            }
            try {
                if (fd != null) fd.close();
            } catch (IOException e) {}
            wr = null;
        }
    }



    /* C++ -> Java methods */

    public static void start() {
        try{
            // Check for context
            if (context == null) {
                Log.w(TAG,"No active Android Context");
            }
            // Check for Bluetooth availability on the Android platform.
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
                Log.w(TAG,"Bluetooth adapter not available");
                return;
            }
            if (!bluetoothAdapter.getProfileProxy(context, btServiceListener,
                            BluetoothProfile.HEALTH)) {
                Log.w(TAG, "Bluetooth profile not available");
                return;
            }
        } catch (RuntimeException e) {
            Log.e(TAG,e.toString());
        }
    }

    public static void registerDatatype(int datatype) {
        Log.d(TAG,"*** registered datatype: " + datatype);
        if (bluetoothHealth != null) {
            bluetoothHealth.registerSinkAppConfiguration("FIXME:GetADecentName", datatype, healthCallback);
        } else {
            // FIXME: Store datatypes for later registration in a map if the
            // bluetoothHealth is not yet connected
        }
    }

    public static void unregisterDatatype(int datatype) {
        Log.d(TAG,"*** unregistered datatype: " + datatype);
// FIXME: Needs a map of registered datatypes...
    }

    public static String getBTAddress(int device) {
        Log.d(TAG,"*** getBTAddress called");
        return "Hello, World! ADDRESS";
    }

    public static String getName(int device) {
        Log.d(TAG,"*** getName called");
        return "Hello, World! NAME";
    }

    public static void disconnect(int device) {
        Log.d(TAG,"*** disconnect called");

    }

    public static void sendMessagePrimary(int device, byte[] message) {
        Log.d(TAG,"*** sendMessagePrimary called");
        // FIXME: Ignoring device number
        if (wr == null) return;
        try {
            wr.write(message);
        } catch (IOException e) {
            Log.e(TAG, "Write exception: " + e.toString());
            // FIXME: Handle exceptions
        }
    }

    public static void sendMessageStreaming(int device, byte[] message) {
        Log.d(TAG,"*** sendMessageStreaming called");
        sendMessagePrimary(device, message);
    }

    /* Java -> C++ methods */

    private static native void connectIndication(int device);
    private static native void disconnectIndication(int device);
    private static native void messageReceived(int device, byte[] message);
}

