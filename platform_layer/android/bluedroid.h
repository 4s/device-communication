#ifndef BLUEDROID_H
#define BLUEDROID_H

#include "Interface/personalhealthdevice.h"
#include "PAL/Interface/basemsg.h"
#include "PAL/Interface/msgreceiver.h"
#include "PAL/Interface/msgsender.h"
#include "4SDC/Interface/core.h"
#include <stdexcept>
#include <QAndroidJniObject>
#include <QAndroidJniEnvironment>
#include <QMap>


using PAL::PersonalHealthDeviceListener;
using PAL::PersonalHealthDevice;
using PAL::HDPDevice;
using PAL::HDPConnector;
using PAL::HDPWrapper;

class HealthDevice;

class HDPConnector:
    public FSYS::MsgReceiver<HDPConnector, HDPWrapper::RegisterDataType>,
    public FSYS::MsgReceiver<HDPConnector, HDPWrapper::UnregisterDataType>,
    public FSYS::MsgReceiver<HDPConnector, fsdc::Core::Ready>
{
public:

    explicit HDPConnector(PersonalHealthDeviceListener &listener);

    virtual ~HDPConnector();

    void receive(fsdc::Core::Ready signal);
    //void start();

    void receive(HDPWrapper::RegisterDataType &signal);
    //void registerDatatype(unsigned dataType);

    void receive(HDPWrapper::UnregisterDataType &signal);
    //void unregisterDatatype(unsigned dataType);

    void handleConnectIndication(int device);
    void handleDisconnectIndication(int device);
    void handleMessageReceived(int device, int len, int8_t *buffer);
    PersonalHealthDeviceListener &listener;
private:
    QMap<int,HealthDevice*> devices;
};

class ConnectionException : public std::runtime_error {
public:
    ConnectionException(const std::string &message) : std::runtime_error(message) {}
};


#endif // BLUEDROID_H
