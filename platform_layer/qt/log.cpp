

#include "../Interface/log.h"
#include <cstdio>
#include <QDebug>
#include <QtGlobal>
#include <QString>

// Make the class private to this module
namespace {
class Logger
{
  std::string moduleName;
public:
  Logger(std::string const &module)
  {
    moduleName = module;
  }

  static void fatal(std::string const &fatal)
  {
    // Note, we use the form ("%s", string) and not (string), as this prevents
    // errors if % is present in the input string
    qFatal("%s", fatal.c_str());
  }

  static void err  (std::string const &err)
  {
    // Note, we use the form ("%s", string) and not (string), as this prevents
    // errors if % is present in the input string
    qCritical("%s", err.c_str());
  }

  static void warn (std::string const &warn)
  {
    // Note, we use the form ("%s", string) and not (string), as this prevents
    // errors if % is present in the input string
    qWarning("%s", warn.c_str());
  }

  static void debug(std::string const &debug)
  {
    // Note, we use the form ("%s", string) and not (string), as this prevents
    // errors if % is present in the input string
    qDebug("%s", debug.c_str());
  }

  static void info (std::string const &info)
  {
    // Note, we use the form ("%s", string) and not (string), as this prevents
    // errors if % is present in the input string
    qDebug("Info: %s", info.c_str());
  }

  std::string formatString(std::string const &string, std::string const &file, int line)
  {
    // Make a 10 char buffer for the line number converted to a string
    // that is 9 char for the number and one for the terminating 0
    char tmpBuffer[10];

    // If the line number is negative or doesn't fit in 9 digits, then we set it
    // to 0
    if(line>999999999 || line < 0) line = 0;

    // Do the conversion
    // Note: We don't use  std::to_string, as that is a C++11 feature
    sprintf(tmpBuffer, "%d", line);

    // Build and return the concatenated string
    return std::string("(" + moduleName + ")\""
                               + string + "\"@" + file + "(" + tmpBuffer + ")");
  }

  void fatal(std::string const &fatal, std::string const &file, int line)
  {
    // Note: We use this->fatal and not just fatal, to distinguish it from the
    // fatal parameter
    this->fatal(formatString(fatal, file, line));
  }

  void err  (std::string const &err,   std::string const &file, int line)
  {
    this->err(formatString(err, file, line));
  }

  void warn (std::string const &warn,  std::string const &file, int line)
  {
    this->warn(formatString(warn, file, line));
  }

  void debug(std::string const &debug, std::string const &file, int line)
  {
    this->debug(formatString(debug, file, line));
  }

  void info (std::string const &info,  std::string const &file, int line)
  {
    this->info(formatString(info, file, line));
  }

};
}

FSYS::Log::Log(std::string const &module){
  me = new Logger(module);
}

FSYS::Log::~Log() {
  delete static_cast<Logger*>(me);
}

void FSYS::Log::fatal(std::string const &fatal)
{
  Logger::fatal(fatal);
}

void FSYS::Log::err  (std::string const &err)
{
  Logger::err(err);
}

void FSYS::Log::warn (std::string const &warn)
{
  Logger::warn(warn);
}

void FSYS::Log::debug(std::string const &debug)
{
  Logger::debug(debug);
}

void FSYS::Log::info (std::string const &info)
{
  Logger::info(info);
}

void FSYS::Log::fatal(std::string const &fatal, std::string const &file, int line)
{
  static_cast<Logger*>(me)->fatal(fatal, file, line);
}

void FSYS::Log::err  (std::string const &err,   std::string const &file, int line)
{
  static_cast<Logger*>(me)->err(err, file, line);
}

void FSYS::Log::warn (std::string const &warn,  std::string const &file, int line)
{
  static_cast<Logger*>(me)->warn(warn, file, line);
}

void FSYS::Log::debug(std::string const &debug, std::string const &file, int line)
{
  static_cast<Logger*>(me)->debug(debug, file, line);
}

void FSYS::Log::info (std::string const &info,  std::string const &file, int line)
{
  static_cast<Logger*>(me)->info(info, file, line);
}
