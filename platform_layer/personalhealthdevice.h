/**
 * @file
 * @brief PAL interface for personal health device communication.
 *
 * PAL interface for communicating with personal health devices.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute, &copy; 2014
 */

#ifndef PERSONALHEALTHDEVICE_H
#define PERSONALHEALTHDEVICE_H

#include "basemsg.h"
#include "msgreceiver.h"
#include "4SDC/Interface/core.h"

#include <memory>
#include <string>
#include <vector>


namespace PAL {



/* ************************************************************ */
/*                       General Interface                      */
/* ************************************************************ */


/**
 * @brief Base class for a transport-layer personal health device.
 *
 * This class is implemented by the transport-layer components below the PAL to
 * model a connected device. Each transport type (USB, Bluetooth, ZigBee)
 * implements different subclasses of this class, each adding the
 * characteristics of that particular transport type.
 *
 * Objects of this class are always created by components below the PAL and
 * managed by smart references. Objects are then handed over to the upper
 * layers using the PersonalHealthDeviceListener class.
 *
 * The transport layer may not reuse the old PersonalHealthDevice if a device
 * is disconnected and then connected again; instead, a new object must be
 * constructed. In general, upper layers should stop using these device objects
 * immediately when a disconnect is reported, and dispose of the smart
 * references as soon as possible.
 */
class PersonalHealthDevice {
public:
    /**
     * @brief Is this device object connected through the transport layer?
     *
     * Reports whether this device object is currently connected through the
     * transport layer to a physical device. The device will be connected when
     * it is first introduced to the upper layers through
     * PersonalHealthDeviceListener::connectIndication().
     *
     * As the PersonalHealthDeviceListener::disconnectIndication() is
     * signalled, this property will transition to <code>false</code>, and it
     * will never change back.
     *
     * @retval true  If this device is currently connected.
     * @retval false If this device is currently unconnected.
     */
    virtual bool isConnected() =0;

    /**
     * @brief Send a message to this device on the primary virtual channel.
     *
     * This method will (attempt to) send a message to the device on the
     * primary virtual channel (which by definition is a reliable transport
     * service). There will be no indication of whether this attempt was
     * succesful or not. In particular, if this device object is currently not
     * connected, the method invocation will be ignored.
     *
     * @param message A pointer to a buffer of bytes which must be transmitted
     *                to the device. The transport layer assumes full ownership
     *                of this buffer (the caller may not touch or change it),
     *                including the obligation to dispose of it when it is no
     *                longer needed.
     */
    virtual void sendMessagePrimary(std::vector<unsigned char> *message) =0;

    /**
     * @brief Disconnect the device.
     *
     * If the device is currently connected, it will be disconnected
     * immediately (and a PersonalHealthDeviceListener::disconnectIndication()
     * will follow), otherwise nothing will happen.
     */
    virtual void disconnect() =0;
};


/**
 * @brief The signature of a handler for the signals emitted by the transport
 * layer.
 *
 * The transport-layer components below the PAL signals the upper protocol
 * layer through the methods defined in this class.
 */
class PersonalHealthDeviceListener {
public:
    /**
     * @brief Indicates the connection of a new device.
     *
     * A new device has been connected at the transport layer and is now
     * available for the protocol layer.
     *
     * @param device The device object, which will be new (never seen before by
     *               the protocol layer) and in the connected state.
     */
    virtual void connectIndication(std::shared_ptr<PersonalHealthDevice> device) =0;

    /**
     * @brief Indicates a device disconnection.
     *
     * A device (which was previously connected using connectIndication()) has
     * been disconnected. It is no longer available for the protocol layer. No
     * more messageReceived() signals will ever be emitted for this object, and
     * PersonalHealthDevice::sendMessage() attempts will be discarded. The
     * protocol layer should forget all about this object and delete all
     * references.
     *
     * @param device The device object, which will no longer be in use.
     */
    virtual void disconnectIndication(std::shared_ptr<PersonalHealthDevice> device) =0;

    /**
     * @brief Indicates that a message was received from a connected device
     *
     * A message arrived from a connected device. This device was previously
     * announced by the connectIndication() signal and has not yet been
     * disconnected.
     *
     * @param device  The source of the message.
     * @param message A pointer to a buffer of bytes with the message payload.
     *                The protocol layer assumes full ownership of this buffer,
     *                including the obligation to dispose of it when it is no
     *                longer needed.
     */
    virtual void messageReceived(std::shared_ptr<PersonalHealthDevice> device,
                                 std::vector<unsigned char> *message) =0;
};





/* ************************************************************ */
/*                      Bluetooth "classic"                     */
/* ************************************************************ */

/**
 * @brief A bluetooth specialization of the PersonalHealthDevice.
 *
 * This abstract class extends the PersonalHealthDevice with properties of the
 * underlying bluetooth Health Device Profile (HDP) device, namely the
 * bluetooth address and name.
 */
class HDPDevice : public PersonalHealthDevice {

    /**
     * @brief The bluetooth address of this device.
     * @return The bluetooth address of this device.
     */
    virtual std::string getBTAddress() =0;

    /**
     * @brief The bluetooth name of this device.
     * @return The bluetooth name of this device.
     */
    virtual std::string getName() =0;

    /**
     * @brief Send a message to this device on a streaming virtual channel.
     *
     * This method will (attempt to) send a message to the device on a
     * streaming virtual channel. If such a channel is not available, it will
     * attempt the primary virtual channel instead. There will be no indication
     * of whether this attempt was succesful or not. In particular, if this
     * device object is currently not connected, the method invocation will be
     * ignored.
     *
     * @param message A pointer to a buffer of bytes which must be transmitted
     *                to the device. The transport layer assumes full ownership
     *                of this buffer (the caller may not touch or change it),
     *                including the obligation to dispose of it when it is no
     *                longer needed.
     */
    virtual void sendMessageStreaming(std::vector<unsigned char> *message) =0;
};

class HDPConnector;


class HDPWrapper
{

public:
    HDPWrapper(PAL::PersonalHealthDeviceListener &listener);
    ~HDPWrapper();

    class RegisterDataType : public FSYS::BaseMsg
    {
    public:
      unsigned dataType;
    };

    class UnregisterDataType : public FSYS::BaseMsg
    {
    public:
      unsigned dataType;
    };


private:
    HDPConnector *connector;
};





/* ************************************************************ */
/*                              USB                             */
/* ************************************************************ */

/**
 * @brief A USB specialization of the PersonalHealthDevice.
 *
 * This abstract class extends the PersonalHealthDevice with properties of the
 * underlying USB Personal Healthcare Device Class (PHDC) device.
 */
class PHDCDevice : public PersonalHealthDevice {

};





/* ************************************************************ */
/*                             ZigBee                           */
/* ************************************************************ */

/**
 * @brief A ZigBee specialization of the PersonalHealthDevice.
 *
 * This abstract class extends the PersonalHealthDevice with properties of the
 * underlying ZigBee Health Care (ZHC) device.
 */
class ZHCDevice : public PersonalHealthDevice {

};


}


#endif // PERSONALHEALTHDEVICE_H
